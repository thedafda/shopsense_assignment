(function(win) {
    
    // Adding to strings prototype for global use.
    String.prototype.supplant = function (o) {
        return this.replace(/{([^{}]*)}/g,
            function (a, b) {
                var r = o[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            }
        );
    };
    
    var touchOrClick = (('ontouchstart' in window) || (window.DocumentTouch && document instanceof DocumentTouch)) ? 'touchstart' : 'click';
    
    win.utils = {
        merge: function (obj1, obj2) {
            obj1 = obj1 || {};
            obj2 = obj2 || {};
            var prop;
            var obj = {};
            for (prop in obj1) {
                if (obj1.hasOwnProperty(prop)) {
                    obj[prop] = obj1[prop];
                }
            }
            for (prop in obj2) {
                if (obj2.hasOwnProperty(prop)) {
                    obj[prop] = obj2[prop];
                }
            }
            return obj;
        },
        extend: function (obj1, obj2) {
            var prop;
            for (prop in obj2) {
                if (obj2.hasOwnProperty(prop)) {
                    obj1[prop] = obj2[prop];
                }
            }
            return obj1;
        },
        findFirst: function (ao, predicateFn) {
            var prop;
            for (prop in ao) {
                if(predicateFn(ao[prop])){
                    return ao[prop];
                }
            }
        },
        getRandomInt: function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        },
        hasTouch: function (argument) {
            return 'ontouchstart' in window;
        },
        touchOrClick: touchOrClick
    };
    
    
    
}(window));



