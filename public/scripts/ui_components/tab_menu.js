(function(win){
    function TabMenu(model) {
        this.model = model;
        this.range = 5;
        this.element = document.createElement('ul');
        this.bindEvents();
        // Listening for change models changes
        this.bindModalEvents();
    }

    TabMenu.prototype.bindModalEvents = function (argument) {
        var that = this;
        that.model.on('loaded', function (argument) {
            that.render();
        });
        that.model.on('changed-selected-category', function () {
            var categories = that.model.categories;
            var selectedCategory = that.model.selectedCategory;
            var index = categories.indexOf(selectedCategory);
            that.setMiddle(index);
        });
        that.model.on('changed-selected-item', function () {

        });
    };

    TabMenu.prototype.render = function () {
        var container = this.element || document.createElement('ul');
        container.innerHTML = '';
        container.classList.add('tab_menu');
        //container.classList.add('ta-container');
        //container.setAttribute('slider-container', '');
        var categories = this.model.categories;
        this.container = container;
        //this.container.appendChild(document.createElement('li'));
        categories.forEach(function (category) {
            var li = document.createElement('li');

            li.setAttribute('valid', '');
            var div = document.createElement('div');
            div.innerHTML = category.name;
            li.appendChild(div);
            li.dataset.name = category.name;
            this.container.appendChild(li);
        }.bind(this));
        //this.container.appendChild(document.createElement('li'));
        this.setMiddle(0);
    };

    TabMenu.prototype.setMiddle = function (index) {
        var nodes = this.container.children;
        if (nodes[index]) {
            this.removeStyle();
            nodes[index].classList.add('selected');
            if (nodes[index - 1]) {
                nodes[index - 1].classList.add('visible');
            }
            if (nodes[index + 1]) {
                nodes[index + 1].classList.add('visible');
            }
        }
    };

    TabMenu.prototype.removeStyle = function (index) {
        Array.prototype.forEach.call(this.container.children, function (node, indx) {
            node.classList.remove('selected');
            node.classList.remove('visible');
        });
    };

    TabMenu.prototype.getElement = function () {
        return this.element;
    };

    TabMenu.prototype.bindEvents = function () {
        var that = this;
        that.containerClickHandler =  function(e) {
			var target = e.target;
			if (target.matches('li[valid], li[valid] *')) {
			    var name = target.dataset.name;
			    that.go({
			        name: name
			    });
			}
		};
		that.element.addEventListener(win.utils.touchOrClick, that.containerClickHandler);
    };

    TabMenu.prototype.go = function (obj) {
        this.model.selectCategory(obj);
    };

    TabMenu.prototype.removeEvents = function (direction) {
        this.element.addEventListener(win.utils.touchOrClick, this.containerClickHandler);
    };

    TabMenu.prototype.destroy = function (direction) {
        this.removeEvents();
        this.model = undefined;
        this.element.parentElement.removeChild(this.element);
    };

    win.UI = win.UI || {};
    win.UI.TabMenu = TabMenu;
}(window));
