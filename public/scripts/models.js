(function(win) {
    
    var baseUrl = "https://demo6558887.mockable.io";
    
    function CategoriesModel(){
        this.categoryUrl = baseUrl + "/category";
    }
    
    win.EventEmitter.extend(CategoriesModel.prototype);
    
    CategoriesModel.prototype.fetchFromServer = function(){
        var that = this;
        win.RESTEngine.get(that.categoryUrl, function getCategoriesSuccess(res) {
            var categories;
            try {
                categories = res.data.objects.all || [];    
            } catch(e) {
                categories = [];
            }
            that.categories = that.parseCategories(categories);
            that.selectCategory(that.categories[0]);
            that.fire('loaded');
        }, function getCategoriesError(errs) {
            that.fire('error');
        });
    };
    
    CategoriesModel.prototype.parseCategories = function(categories) {
        var that = this;
        categories.forEach(function(mobj) {
            mobj.link = that.categoryUrl + '/' + mobj.name.toLowerCase();
        });
        return categories;
    };
    
    CategoriesModel.prototype.parseItems = function(items, pobj) {
        items.forEach(function(mobj) {
            var subPath = pobj.name.toLowerCase();
            mobj.imgLink =  'imgs/cloths/' + subPath + '/' + mobj.sku + '.png';
            mobj.price = win.utils.getRandomInt(100, 1000);
            mobj.size = 'M, L, XL, XXL';
        });
        return items;
    };
    
    CategoriesModel.prototype.fetchItems = function () {
        var that = this;
        if(that.selectedCategory && that.selectedCategory.items){
            that.fire('changed-selected-category');
            return;
        }
        win.RESTEngine.get(that.selectedCategory.link, function getCategoriesSuccess(res) {
            var items;
            try {
                items = res.data.objects || [];    
            } catch(e) {
                items = [];
            }
            that.selectedCategory.items = that.parseItems(items, that.selectedCategory);
            that.selectedCategory.selectedItem = that.selectedCategory.items[0];
            that.selectedCategory.selectedItem.selected = true;
            that.fire('changed-selected-category');
            that.fire('changed-selected-item');
        }, function getCategoriesError(errs) {});
    };
    
    CategoriesModel.prototype.selectCategory = function(options){
        var that = this;
        var currentCategory = win.utils.findFirst(this.categories, function(o){
            return o.name === options.name;
        });
        var lastSelected = win.utils.findFirst(this.categories, function(o){
            return o.selected === true; 
        });
        if(currentCategory){
            currentCategory.selected = true;
            if(lastSelected) {
                delete lastSelected.selected;
            }
            that.selectedCategory = currentCategory;
            that.fetchItems();
        }
    };
    
    CategoriesModel.prototype.selectItem = function(options){
        var that = this;
        if(that.selectedCategory){
            var currentItem = win.utils.findFirst(that.selectedCategory.items, function(o){
                return o.name === options.name || o.sku === options.sku; 
            });
            var lastSelected = win.utils.findFirst(that.selectedCategory.items, function(o){
                return o.selected === true; 
            });
            if(currentItem){
                if(lastSelected) {
                    delete lastSelected.selected;
                }
                currentItem.selected = true;
                that.selectedCategory.selectedItem = currentItem;
                that.fire('changed-selected-item');
            }
        }
    };
    
    CategoriesModel.prototype.selectItemDirection = function(direction){
        var nidx = this.selectedCategory.items.indexOf(this.selectedCategory.selectedItem);
        delete this.selectedCategory.selectedItem.selected;
        if(direction === 'next') {
            nidx = nidx + 1;
        } else {
            nidx = nidx - 1;
        }
        var selectedItem = this.selectedCategory.items[nidx];
        if(selectedItem){
            selectedItem.selected = true;
            this.selectedCategory.selectedItem = selectedItem;
            this.fire('changed-selected-item');
        }
    };
    
    CategoriesModel.prototype.get = function(){
        return this.model;
    };
    
    win.CategoriesModel = CategoriesModel;
}(window));
