module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      build: {
        src: [
          // Core
          'public/scripts/core/helpers.js',
          'public/scripts/core/EventEmitter.js',
          'public/scripts/core/RESTEngine.js',
          // Models
          'public/scripts/models.js',
          // UI Components
          'public/scripts/ui_components/3d_carousel.js',
          'public/scripts/ui_components/desc_menu.js',
          'public/scripts/ui_components/tab_menu.js',
          'public/scripts/ui_components/category_card.js',
          // Main
          'public/scripts/main.js'
        ],
        dest: 'dist/main.min.js'
      }
    },
    cssmin:{
      build: {
        files: {
          'dist/styles.min.css': [
            'public/styles/reset.css',
            'public/styles/main.css',
            'public/styles/ui_components/**/*.css' ]
        }
      }
    },
    copy: {
      build: {
        files: [
          {
            expand: true,
            cwd: 'public',
            src: ['imgs/**'],
            dest: 'dist'
          }
        ]
      }
    },
    processhtml: {
      build: {
        files: {
          'dist/index.html': ['public/index.html']
        }
      }
    },
    jshint: {
      all: ['public/**/*.js']
    },
    clean:{
      build: {
        src: ["dist"]
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-processhtml');

  grunt.registerTask('default', ['uglify']);
  grunt.registerTask('build', ['jshint', 'clean', 'uglify', 'cssmin', 'processhtml', 'copy']);

};