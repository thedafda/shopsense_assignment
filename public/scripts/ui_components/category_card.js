(function(win){
    function CategoryCard(model) {
        this.model = model;
        this.element = document.createElement('div');
        this.bindEvents();
        // Listening for change models changes
        this.render();
        this.bindModalEvents();
        this.model.fetchFromServer();
    }
    
    CategoryCard.prototype.bindModalEvents = function () {
        var that = this;
        that.model.on('loaded', function(){
            //that.render();
        });
        /*this.model.on('changed-selected-category', function () {
            that.categories = this.model.categories;
        });*/
    };
    
    CategoryCard.prototype.templateHTML = [
    '<div category-card class="category-card">',
        '<div tab-menu-container class="tab-menu-container"></div>',
        '<div carousel-container class="carousel-container"></div>',
        '<div desc-menu-container class="desc-menu-container"></div>',
    '</div>'].join('');
    
    CategoryCard.prototype.render = function () {
        var container = this.element || document.createElement('div');
        var compiledHTML = this.templateHTML.supplant(this.categories);
        container.innerHTML = compiledHTML;
        
        this.tabMenu = new win.UI.TabMenu(this.model);
        container.querySelector('[tab-menu-container]').appendChild(this.tabMenu.element);
        this.descMenu = new win.UI.DescMenu(this.model);
        container.querySelector('[desc-menu-container]').appendChild(this.descMenu.element);
        this.carousel = new win.UI.Carousel(this.model);
        container.querySelector('[carousel-container]').appendChild(this.carousel.element);
    };
    
    CategoryCard.prototype.getElement = function () {
        return this.element;
    };
    
    CategoryCard.prototype.bindEvents = function () {
    };
    
    CategoryCard.prototype.removeEvents = function (direction) {
    };
    
    CategoryCard.prototype.destroy = function (direction) {
        this.removeEvents();
        this.model = undefined;
        this.element.parentElement.removeChild(this.element);
    };
    
    window.UI = window.UI || {};
    window.UI.CategoryCard = CategoryCard;
}(window));
