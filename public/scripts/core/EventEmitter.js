(function (win) {
    function EventEmitter(){}
    EventEmitter.prototype	= {
    	on: function(event, fn){
    		this._events = this._events || {};
    		this._events[event] = this._events[event]	|| [];
    		this._events[event].push(fn);
    	},
    	off: function(event, fn){
    		this._events = this._events || {};
    		if( event in this._events === false  )	return;
    		this._events[event].splice(this._events[event].indexOf(fn), 1);
    	},
    	fire: function(event){
    		this._events = this._events || {};
    		if( event in this._events === false  )	return;
    		for(var i = 0; i < this._events[event].length; i++){
    			this._events[event][i].apply(this, Array.prototype.slice.call(arguments, 1));
    		}
    	}
    };
    
    win.EventEmitter = EventEmitter;
    win.EventEmitter.extend = function (obj) {
        win.utils.extend(obj, EventEmitter.prototype);
    };
}(window));
