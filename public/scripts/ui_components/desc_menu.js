(function(win){
    function DescMenu(model) {
        this.model = model;
        this.element = document.createElement('div');
        this.bindEvents();
        // Listening for change models changes
        this.bindModalEvents();
    }

    DescMenu.prototype.bindModalEvents = function () {
        var that = this;
        this.model.on('changed-selected-category', function () {
            that.render();
        });
        this.model.on('changed-selected-item', function () {
            that.render();
        });
    };

    DescMenu.prototype.templateHTML = [
    '<div desc-menu class="desc-menu">',
        '<div class="item-name">{name}</div>',
            '<div class="item-info">',
                '<div class="item-stock-size">',
                '<div class="lt" go="prev">&lt;</div>',
                '<div class="sizes">{size}</div>',
                '<div class="gt" go="next">&gt;</div>',
            '</div>',
            '<div class="item-price">',
                '<div>&#x20B9 {price}</div>',
            '</div>',
            '<div class="item-more-btn">',
                '<div>More</div>',
                '<div>Details</div>',
            '</div>',
        '</div>',
    '</div>'].join('');

    DescMenu.prototype.render = function () {
        var container = this.element || document.createElement('div');
        var compiledHTML = this.templateHTML.supplant(this.model.selectedCategory.selectedItem);
        container.innerHTML = compiledHTML;
    };

    DescMenu.prototype.getElement = function () {
        return this.element;
    };

    DescMenu.prototype.bindEvents = function () {
        var that = this;
        that.containerClickHandler =  function(e) {
			var target = e.target;
			if (target.matches('[go]')) {
			    var direction = target.getAttribute('go');
			    that.go(direction);
			}
		};
		that.element.addEventListener(win.utils.touchOrClick, that.containerClickHandler);
    };

    DescMenu.prototype.go = function (direction) {
        this.model.selectItemDirection(direction);
    };

    DescMenu.prototype.removeEvents = function (direction) {
        this.element.addEventListener(win.utils.touchOrClick, this.containerClickHandler);
    };

    DescMenu.prototype.destroy = function (direction) {
        this.removeEvents();
        this.model = undefined;
        this.element.parentElement.removeChild(this.element);
    };

    win.UI = win.UI || {};
    win.UI.DescMenu = DescMenu;
}(window));
