(function(win){
    function Carousel(model) {
        this.model = model;
        this.range = 4;
        this.element = document.createElement('div');
        this.bindEvents();
        // Listening for change models changes
        this.bindModalEvents();
    }
    
    Carousel.prototype.bindModalEvents = function (argument) {
        var that = this;
        that.model.on('changed-selected-category', function () {
            that.render();
        });
        that.model.on('changed-selected-item', function () {
            var items = that.model.selectedCategory.items;
            var selectedItem = that.model.selectedCategory.selectedItem;
            var index = items.indexOf(selectedItem);
            that.setMiddle(index);
        });
    };
    
    Carousel.prototype.render = function () {
        var container = this.element || document.createElement('div');
        container.innerHTML = '';
        container.classList.add('slider-container');
        container.setAttribute('slider-container', '');
        var items = this.model.selectedCategory.items;
        this.sliderContainer = container;
        items.forEach(function (item) {
            var img = document.createElement('img');
            img.src = item.imgLink;
            var div = document.createElement('div');
            div.appendChild(img);
            this.sliderContainer.appendChild(div);
        }.bind(this));
        this.setMiddle(0);
    };
    
    Carousel.prototype.setMiddle = function (midIndex) {
        var nodes = this.sliderContainer.children;
        var range = this.range;
        
        if(midIndex > nodes.length - 1 || midIndex < 0){ return; }
        
        this.resetStyle();
        var totalRange = range * 2;
        var onePart = 100 / (totalRange + 1);
        var middleNode = nodes[midIndex];
        if (middleNode) {
            middleNode.classList.add('middle');
            middleNode.style.left = range * onePart + '%';
            middleNode.style.opacity = 1;
            middleNode.style.transform = 'translateZ(' + onePart * range + 'px) scale(1.1)';
        }
        for (var i = 1; i <= range; i++) {
            var fromStart = midIndex - i;
            var fromBack = midIndex + i;
    
            var fs = nodes[fromStart];
            if (fs) {
                fs.classList.add('visible');
                fs.style.left = (range - i) * onePart + '%';
                fs.style.opacity = (range - i) * onePart / 100 + 0.4;
                fs.style.transform = 'translateZ(' + (range - i) * onePart + 'px)';
            }
            var fb = nodes[fromBack];
            if (fb) {
                fb.classList.add('visible');
                fb.style.left = (range + i) * onePart + '%';
                fb.style.opacity = (range - i) * onePart / 100 + 0.4;
                fb.style.transform = 'translateZ(' + (range - i) * onePart + 'px)';
            }
        }
    };
    
    Carousel.prototype.resetStyle = function () {
        Array.prototype.forEach.call(this.sliderContainer.children, function (node, indx) {
            node.classList.remove('middle');
            node.classList.remove('visible');
            //node.style.left = 0;
            node.style.opacity = 0;
            node.style.transform ='';
        });
    };
    
    Carousel.prototype.getElement = function () {
        return this.element;
    };
    
    Carousel.prototype.bindEvents = function () {
        var that = this;
        that.containerClickHandler =  function(e) {
			var target = e.target;
			if (target.matches('[go]')) {
			    var direction = target.getAttribute('go');
			    that.go(direction);
			} 
		};
		that.element.addEventListener(win.utils.touchOrClick, that.containerClickHandler);
    };
    
    Carousel.prototype.go = function (direction) {
        this.model.selectItemDirection(direction);
    };
    
    Carousel.prototype.removeEvents = function (direction) {
        this.element.addEventListener(win.utils.touchOrClick, this.containerClickHandler);
    };
    
    Carousel.prototype.destroy = function (direction) {
        this.removeEvents();
        this.model = undefined;
        this.element.parentElement.removeChild(this.element);
    };
    
    win.UI = win.UI || {};
    win.UI.Carousel = Carousel;
}(window));
