(function (win) {
    var RESTEngine = {
        XHR: XMLHttpRequest,
        utils: {
            setHeaders: function (request, headersObj) {
                for(var headerProp in headersObj){
                    request.setRequestHeader(headerProp, headersObj[headerProp]);
                }
            }  
        },
        ajax: function ajax(options){
            options.method = options.method || 'GET';
            options.body = options.body || '';
            
            var request = new this.XHR();
            request.open(options.method, options.url, !options.sync);
            if(options.headers){
                this.utils.setHeaders(request, options.headers);
            }
            request.onreadystatechange = function() {
                if (request.readyState == 4) {
                    if(request.status == 200) {
                        var data = JSON.parse(request.responseText);
                        if(options.success) {
                            options.success({
                                status: Number(request.status),
                                statusText: request.statusText,
                                data: data
                            });
                        }
                        return;
                    } else {
                        if(options.error) {
                            options.error({
                                status: Number(request.status),
                                statusText: request.statusText
                            });
                        }
                    }
                }
            };
            
            request.send(options.body);
        },
        get: function get(url, successCallback, errorCallback, options) {
            var opts = window.utils.merge(options, {
                url: url,
                method: 'GET',
                success: successCallback,
                error: errorCallback
            });
            this.ajax(opts);
        }
    };
    
    win.RESTEngine = RESTEngine;
})(window);